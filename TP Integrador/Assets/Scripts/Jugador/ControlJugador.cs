﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    //Variables Publicas
    public float rapidezDesplazamiento;
    public Animator anim;
    public Text Mision1;
    public Text Misiones;
    public Text Completado;
    public GameObject Cofre;

    // Variables privadas
    private int cofres = 0;


     void Awake()
    {
        anim = GetComponent<Animator>();      
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    void Update()
    {
           
            float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
            float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(new Vector3(movimientoCostados, 0, movimientoAdelanteAtras));

        if(movimientoAdelanteAtras!=0 || movimientoCostados!=0)
        {
            anim.SetBool("Correr", true);
        } else
        {
            anim.SetBool("Correr", false);
        }

        if (Input.GetKey("Escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }



    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Lanza"))
        {
            print("Danho");
        }

        if (other.gameObject.CompareTag("Cofre")==true)
        {
            cofres++;
            
            Mision1.text = $"Conseguiste {cofres} cofres";


            if (cofres == 7)
            {
                Destroy(Mision1);
                Destroy(Misiones);
                Completado.text = "Lo has conseguido, una nave esta viniendo por ti, buena suerte!!!!";
                SceneManager.LoadScene("Final");
            }
        }
      
        
    }

   

   
}
