﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GestorPersistencia : MonoBehaviour
{
    public static GestorPersistencia instancia;
    public DataPersistencia data;
    public GameObject jugador;
    string archivoDatos = "InfoJuego.dat";

    void Awake()
    {
        if (instancia == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
        {
            Destroy(this.gameObject);
        }
        CargarDataPersistencia();
    }

    public void CargarDataPersistencia()
    {
        string filepath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filepath))
        {
            FileStream file = File.Open(filepath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;
            file.Close();

            jugador.transform.position = new Vector3(data.posicionJugador.x, data.posicionJugador.y, data.posicionJugador.z);

            Debug.Log("Datos Cargados");
        }
    }

    public void GuardarDataPersistencia()
    {
        data.posicionJugador = new DataPersistencia.Punto(jugador.transform.position);
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Datos Guardados");
    }

    void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.G))
        {
            GuardarDataPersistencia();
        }
    }
}

[System.Serializable]
public class DataPersistencia
{
    public Punto posicionJugador;


    public DataPersistencia()
    {

    }

    [System.Serializable]
    public class Punto
    {
        public float x;
        public float y;
        public float z;

        public Punto(Vector3 p)
        {
            x = p.x;
            y = p.y;
            z = p.z;
        }

        public Vector3 aVector() 
        {
            return new Vector3(x, y, z);
        }
    }
}
