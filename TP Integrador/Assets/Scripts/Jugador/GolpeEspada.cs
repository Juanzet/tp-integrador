﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolpeEspada : MonoBehaviour
{
    public Rigidbody rb;
    public Animator anim;

 
    void Awake()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>(); 
    }
 

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            anim.SetBool("Ataque", true);
        } 
        else
        {
            anim.SetBool("Ataque", false);
        }


        if (Input.GetMouseButton(1))
        {
            anim.SetBool("Bloqueando", true);
        }
        else
        {
            anim.SetBool("Bloqueando", false);
        }
    }

    
}
