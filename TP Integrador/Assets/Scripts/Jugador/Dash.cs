﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;

public class Dash : MonoBehaviour
{
    public float dashVelocidad;
    bool estaDasheando;
    Rigidbody rb;
    public Animator anim;
    public GameObject dashEfecto;
    Vector3 movimientoDireccion;

    private ParticleSystem particula;


     void Awake()
    {
        particula = GetComponent<ParticleSystem>();
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();  
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
            estaDasheando = true;

        if(estaDasheando == true)
        {

            anim.SetBool("Dash", true);
        } else
        {
            
            anim.SetBool("Dash", false);
        }
    }

    private void FixedUpdate()
    {
        if (estaDasheando)
            Dasheando();
    }

    private void Dasheando()
    {
        rb.AddForce(transform.forward * dashVelocidad, ForceMode.Impulse);
        estaDasheando = false;

        GameObject efecto = Instantiate(dashEfecto, Camera.main.transform.position, dashEfecto.transform.rotation);
        efecto.transform.parent = Camera.main.transform;
        efecto.transform.LookAt(transform);

    }

  
   
}
