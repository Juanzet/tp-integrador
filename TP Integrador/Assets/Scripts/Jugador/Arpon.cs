﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arpon : MonoBehaviour
{
    private LineRenderer lr;
    private Vector3 puntoArpon;
    public LayerMask cualEsArponeable;
    public Transform Arponclip, camara, jugador;
    private float distanciaMaxima = 100f;
    private SpringJoint joint;

    void Awake()
    {
        lr = GetComponent<LineRenderer>();
    }

    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            Arponeable();
        } else if (Input.GetButtonDown("Fire1"))
        {
            NoArponeable();
        }
        
    }

    void LateUpdate()
    {
        DibujarArpon();
    }

    void Arponeable()
    {
        RaycastHit hit;
        if (Physics.Raycast(origin: camara.position, direction: camara.forward, out hit, distanciaMaxima))
        {
            puntoArpon = hit.point;
            joint = jugador.gameObject.AddComponent<SpringJoint>();
            joint.autoConfigureConnectedAnchor = false;
            joint.connectedAnchor = puntoArpon;

            float distanciaDelPunto = Vector3.Distance(jugador.position, puntoArpon);

             //la distancia que el arpon va a tener
            joint.maxDistance = distanciaDelPunto * 0.8f;
            joint.minDistance = distanciaDelPunto * 0.25f;

            //ajustar estos valores desde el juego
            joint.spring = 4.5f;
            joint.damper = 7f;
            joint.massScale = 4.5f;

            lr.positionCount = 2;
        }

    }

    void DibujarArpon()
    {
        if (!joint) return;
        lr.SetPosition(0, Arponclip.position);
        lr.SetPosition(1, puntoArpon);
    }

    public bool Agarrado ()
    {
        return joint != null;
    }

    public Vector3 ConseguirPuntoAgarre()
    {
        return puntoArpon;
    }

    void NoArponeable()
    {
        lr.positionCount = 0;
        Destroy(joint);
    }
}
