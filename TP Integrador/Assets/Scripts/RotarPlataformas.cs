﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarPlataformas : MonoBehaviour
{
    public Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        transform.Rotate(new Vector3(0, 30, 0) * Time.deltaTime);
    }
}
