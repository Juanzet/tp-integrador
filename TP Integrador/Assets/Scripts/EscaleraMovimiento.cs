﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscaleraMovimiento : MonoBehaviour
{
    public GameObject jugador;
    public Animator anim;


    void Awake()
    {
        anim = GetComponent<Animator>();
        jugador = GetComponent<GameObject>();

    }

     
    void Update()
    {

        JugadorMoviendose();

    }



    void JugadorMoviendose()
    {
        if (Input.GetButton("Vertical") || Input.GetButton("Horizontal"))
        {
            anim.SetBool("EscaleraMov", true);
        } else
        {
            //anim.SetBool("Escalera", false);
        }
    }
}
