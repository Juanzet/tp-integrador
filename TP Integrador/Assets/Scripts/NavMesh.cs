﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMesh : MonoBehaviour
{
    public Transform destino;
    private NavMeshAgent agente;

    void Start()
    {
      agente = GetComponent<NavMeshAgent>();
        
    }

    
    void Update()
    {
        agente.destination = destino.position;
    }
}
