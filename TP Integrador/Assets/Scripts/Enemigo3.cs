﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo3 : MonoBehaviour
{
    public GameObject Proyectil;
    public Camera camaraPrimeraPersona;
    private int hp;
    private GameObject jugador;
    public int contProyectiles;
  

    void Start()
    {
        hp = 100;
        
        
    }
   
    void Update()
    {
            
        GameObject pro;
        Ray rayo = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        pro = Instantiate(Proyectil, rayo.origin, transform.rotation);
        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb.AddForce(camaraPrimeraPersona.transform.forward * 110, ForceMode.Impulse);
        Destroy(pro, 1);
      
    }

    public void recibirDamage()
    {
        hp = hp - 25;
        if (hp <= 0)
        {
            desaparecer();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDamage();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }
}
