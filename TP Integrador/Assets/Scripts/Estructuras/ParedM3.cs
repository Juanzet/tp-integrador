﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ParedM3 : MonoBehaviour
{
    bool tengoQueBajar = false;
    int rapidez = 5;

    void Update()
    {
        if (transform.position.z >= 665.9455f)
        {
            tengoQueBajar = true;
        }
        if (transform.position.z <= 641.3f)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Jugador"))
        {
            SceneManager.LoadScene("Perdiste");
        }
    }

    void Subir()
    {
        transform.position += transform.forward * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    }
}
