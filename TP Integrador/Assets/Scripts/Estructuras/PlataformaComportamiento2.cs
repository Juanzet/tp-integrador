﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaComportamiento2 : MonoBehaviour
{
    bool tengoQueBajar = false;
    int rapidez = 5;

    void Update()
    {
        if (transform.position.z >= 668.83f)
        {
            tengoQueBajar = true;
        }
        if (transform.position.z <= 650.32f)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }


    void Subir()
    {
        transform.position += transform.forward * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    }
}
