﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPlataforma : MonoBehaviour
{
    public Rigidbody plataformaRb;
    public Transform[] posicionPlataforma;
    public float velocidadPlataforma;

    private int posicionActual = 0;
    private int siguientePosicion = 1;
     
    void Update()
    {
        movPlataforma();
    }

    void movPlataforma()
    {
        plataformaRb.MovePosition(Vector3.MoveTowards(plataformaRb.position, posicionPlataforma[siguientePosicion].position, velocidadPlataforma * Time.deltaTime));

        if (Vector3.Distance(plataformaRb.position, posicionPlataforma[siguientePosicion].position) <= 0)
        {
            posicionActual = siguientePosicion;
            siguientePosicion++;

            if (siguientePosicion > posicionPlataforma.Length - 1)
            {
                siguientePosicion = 0;
            }

        }
    }
}
