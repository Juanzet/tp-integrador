﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public int vida;
    public int dañoEspada;
    public Animator animacion;

    void Start()
    {

    }


    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Espada")
        {
            if (animacion != null)
            {
                animacion.Play(" ");
            }
            vida -= dañoEspada;     

        }
        
        if (vida < 0)
        {
            Destroy(gameObject);
        }
    }
}
