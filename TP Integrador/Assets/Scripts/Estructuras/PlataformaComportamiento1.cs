﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaComportamiento1 : MonoBehaviour
{
    bool tengoQueBajar = false;
    int rapidez = 5;

    void Update()
    {
        if (transform.position.y >= 158.81f) 
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= 150.39f)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }


    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
