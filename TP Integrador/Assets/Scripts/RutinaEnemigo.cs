﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RutinaEnemigo : MonoBehaviour
{
    public int rutina;
    public float cronometro;
    public Animator anim;
    public GameObject jugador;
    public Quaternion angulo;
    public float grado;

    void Awake()
    {
        anim = GetComponent<Animator>();
        jugador = GameObject.Find("Player");
    }

     
    void Update()
    {
        comportamientoEnemigo();
    }


    public void comportamientoEnemigo()
    {
        if (Vector3.Distance(transform.position, jugador.transform.position) > 2)
        {

            cronometro += 1 * Time.deltaTime;
            if (cronometro >= 4)
            {
                transform.Translate(Vector3.forward * 1 * Time.deltaTime);
                anim.SetBool("Caminando", true);
                rutina = Random.Range(0, 2);
                cronometro = 0;

                switch (rutina)
                {
                    case 0:
                        anim.SetBool("Caminando", false);
                        break;

                    case 1:
                        grado = Random.Range(0, 360);
                        angulo = Quaternion.Euler(0, grado, 0);
                        rutina++;
                        break;

                    case 2:
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, angulo, 0.5f);
                        transform.Translate(Vector3.forward * 1 * Time.deltaTime);
                        anim.SetBool("Caminando", true);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                if (Vector3.Distance(transform.position, jugador.transform.position) > 1)
                {
                    var lookPos = jugador.transform.position - transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 3);

                    anim.SetBool("Caminando", true);
                    transform.Translate(Vector3.forward * 2 * Time.deltaTime);

                    anim.SetBool("Atacando", false);
                } 
                else
                {
                    anim.SetBool("Caminando", false);
                    anim.SetBool("Atacando", true);

                    
                }
            }
        }
    }

}
