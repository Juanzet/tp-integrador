﻿using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour
{

    
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public void EscenaJuego()
    {
        SceneManager.LoadScene("Juego");
    }

    public void EscenaOpciones()
    {
        SceneManager.LoadScene("Opciones");
    }

    public void SalirDelJuego()
    {
        Debug.Log("Sali del juego");
        Application.Quit();
    }
}
