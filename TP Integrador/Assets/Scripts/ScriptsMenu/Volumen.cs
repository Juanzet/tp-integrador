﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Volumen : MonoBehaviour
{
    public Slider slider;
    public float sliderValor;
    public Image imagenMute;

    
    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("volumenAudio", 0.5f);
        AudioListener.volume = slider.value;
        RevisarVolumen();
    }

    public void ChangeSlider(float valor)
    {
        sliderValor = valor;
        PlayerPrefs.SetFloat("volumenAudio", sliderValor);
        AudioListener.volume = slider.value;
        RevisarVolumen();
    }   

   public void RevisarVolumen()
    {
        if (sliderValor == 0)
        {
            imagenMute.enabled = true;
        } else
        {
            imagenMute.enabled = false;
        } 
        
    }
}
