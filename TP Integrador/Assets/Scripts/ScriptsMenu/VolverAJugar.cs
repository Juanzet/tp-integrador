﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VolverAJugar : MonoBehaviour
{
    public void ReiniciarJuego()
    {
        SceneManager.LoadScene("Juego");
    }
}
