﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCalculador : MonoBehaviour
{
    public Mesh mesh;
    public float altura;
    public Material mat;
    public int matIndex;

    
    void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        mat = GetComponent<MeshRenderer>().materials[matIndex];
    }


    void Update()
    {
        altura = mesh.bounds.size.y;

        mat.SetFloat("_altura", altura);
    }
}
