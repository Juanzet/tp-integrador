﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparos : MonoBehaviour
{
    public GameObject bala;
    public Camera Camara;
    private GameObject jugador;
    public int rapidez = 5;

    void Start()
    {

        jugador = GameObject.Find("Player");
        StartCoroutine("Esperar");
    }

    private void Update()
    {
        transform.LookAt(jugador.transform);



    }
    IEnumerator Esperar()
    {
        bool a = true;
        while (a)
        {
            yield return new WaitForSeconds(1f);
            Ray rayo = Camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(bala, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(Camara.transform.forward * 75, ForceMode.Impulse);

            Destroy(pro, 5);


        }

    }
}